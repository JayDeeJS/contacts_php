const contactsTBody = document.querySelector("#contactsTBody");

//CREATE
const modal = document.querySelector('.modal');
const modalClose = document.querySelector('.modalClose');
const modalDialog = document.querySelector('.modalDialog');

const addBtn = document.querySelector('.addBtn');

addBtn.addEventListener('click', () => {
    modal.style.display = 'block';
})

modalClose.addEventListener('click', () => {
    modal.style.display = 'none';
})

modal.addEventListener('click', () => {
    modal.style.display = 'none';
})

modalDialog.addEventListener('click', (e) => {
    e.stopPropagation();
})

//DELETE
document.addEventListener("DOMContentLoaded", function () {
    const deleteButtons = document.querySelectorAll(".deleteBtn");

    deleteButtons.forEach((deleteButton) => {
        deleteButton.addEventListener("click", function () {
            const contactId = deleteButton.getAttribute("data-contact-id");

            const xhr = new XMLHttpRequest();
            xhr.open("GET", `config/delete.php?id=${contactId}`, true);

            xhr.onreadystatechange = function () {
                if (xhr.readyState === 4 && xhr.status === 200) {
                    const contactRow = deleteButton.closest("tr");
                    contactRow.remove();
                }
            }
            xhr.send();
        })
    })
});
