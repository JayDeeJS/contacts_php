<?php
    require_once 'config/database.php';
    
    $id = $_GET['id'];
    $contact = mysqli_query($connect, "SELECT * FROM `contacts` WHERE `id` = '$id' ");
    $contact = mysqli_fetch_assoc($contact);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Редактировать контакт</title>
    <link rel="stylesheet" href="css/app.css">
</head>
<body>
    <div class="modalUpdate">
        <div class="modalUpdateDialog">
            <div class="modalUpdateContent">
                <form id="modalUpdateForm" action="config/update.php" method="post">
                    <div class="modalUpdateClose">
                        <a href="index.php">&times;</a>
                    </div>
                    <input type="hidden" name="id" value="<?= $id ?>">
                    <input value="<?= $contact['cont_name'] ?>" name="cont_name" type="text">
                    <input value="<?= $contact['cont_number'] ?>" name="cont_number" type="text">
                    <input value="<?= $contact['cont_relation'] ?>" name="cont_relation" type="text">
                    <button type="submit">Редактировать контакт</button>
                </form>
            </div>
        </div>
    </div>
</body>

<script>
    //UPDATE
    const modalUpdate = document.querySelector('.modalUpdate');
    const modalUpdateClose = document.querySelector('.modalUpdateClose');
    const modalUpdateDialog = document.querySelector('.modalUpdateDialog');
    const redactBtn = document.querySelectorAll('.redactBtn');

    redactBtn.forEach((redactBtn) => {
        redactBtn.addEventListener('click', (e) => {
            e.preventDefault();
            modalUpdate.style.display = 'block';
        })
    })

    modalUpdateClose.addEventListener('click', () => {
        modalUpdate.style.display = 'none';
    })

    modalUpdateDialog.addEventListener('click', (e) => {
        e.stopPropagation();
    })
</script>

</html>