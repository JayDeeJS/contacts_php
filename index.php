<?php
    require_once 'config/database.php';
    $contacts = mysqli_query($connect, "SELECT * FROM `contacts`");
    $contacts = mysqli_fetch_all($contacts);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Телефонный справочник</title>
    <link rel="stylesheet" href="css/app.css">
</head>
<body>
    <header>
        <div>Телефонный справочник</div>
        <div>
            <div><span>backend:</span> PHP v8.2.9, MySQL v8.0.34</div>
            <div><span class="blue">frontend:</span> HTML, CSS + SASS, JS</div>
        </div>
    </header>
    
    <div class="modal">
        <div class="modalDialog">
            <div class="modalContent">
                <form id="modalContentForm" action="config/create.php" method="post">
                    <div class="modalClose">&times;</div>
                    <input name="cont_name" class="modalName" type="text" placeholder="ФИО">
                    <input name="cont_number" class="modalNumber" type="number" placeholder="Номер телефона">
                    <input name="cont_relation" class="modalRelation" type="text" placeholder="Кем приходится">
                    <button type="submit">Создать контакт</button>
                </form>
            </div>
        </div>
    </div>

    <main class="wrapper">
        <table>
            <thead>
                <button class="addBtn">Добавить контакт</button>
                <tr>
                    <td>ФИО</td>
                    <td>Контактный телефон</td>
                    <td>Кем приходится</td>
                    <td>Управление контактом</td>
                </tr>
            </thead>
            <tbody id="contactsTBody">
                <?php
                    foreach($contacts as $contact) {
                        ?>
                            <tr>
                                <td><?= $contact[1] ?> </td>
                                <td><?= $contact[2] ?> </td>
                                <td><?= $contact[3] ?> </td>
                                <td class="controls">
                                    <button class="redactBtn">
                                        <a href="update.php?id=<?= $contact[0] ?>">Редактировать</a>
                                    </button>
                                    <!-- <button class="deleteBtn">
                                        <a href="config/delete.php?id=<?= $contact[0] ?>">Удалить</a>
                                    </button> -->
                                    <button class="deleteBtn" data-contact-id="<?= $contact[0] ?>">Удалить</button>
                                </td>
                            </tr>
                        <?php
                    }
                ?>
            </tbody>
        </table>
    </main>
</body>

<script src="js/app.js"></script>

</html>